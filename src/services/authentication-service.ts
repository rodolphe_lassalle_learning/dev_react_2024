export default class AuthenticationService {
  static isAuthenticated: boolean = false;

  static login(uername: string, password: string): Promise<boolean> {
    const isAuthenticated = (uername === 'pikachu' && password === 'pikachu');

    return new Promise(resolve => {
      setTimeout(() => {
        this.isAuthenticated = isAuthenticated;
        resolve(isAuthenticated);
      }, 1000);
    });
  }
}