import React, { FunctionComponent, useState, useEffect } from 'react';
import Pokemon from '../models/pokemon';
/**import POKEMONS from '../models/mock-pokemon';*/
import PokemonCard from '../components/pokemon-card';
import PokemonService from '../services/pokemon-service';
import { Link } from 'react-router-dom';
import PokemonSearch from '../components/pokemon-search';
// import usePokemons from '../hooks/pokemon.hook';
  
const PokemonList: FunctionComponent = () => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  
  useEffect(() => {
    /** setPokemons(POKEMONS);*/
    /**fetch('http://localhost:3001/pokemons')
      .then(response => response.json())
      .then((pokemons) => {
        setPokemons(pokemons);
      });
      */
    PokemonService.getPokemons().then(pokemons => setPokemons(pokemons));
  }, []);

  /** Utilisation du hook pokemon
  * const pokemonsHook = usePokemons();
  */
  return (
    <div>
      <h1 className="center">Pokédex </h1>
      <div className="container"> 
        <div className="row"> 
        <PokemonSearch />
        {pokemons.map(pokemon => (
          <PokemonCard key={pokemon.id} pokemon={pokemon} />
        ))}
        {/* {pokemonsHook.map(pokemon => (
          <PokemonCard key={pokemon.id} pokemon={pokemon} />
        ))} */}
        </div>
      </div>
      <Link to={`/pokemon/add`} className="btn-floating btn-large waves-effect waves-light z-depth-3 red">
          <i className="material-icons">add</i>
        </Link>
    </div> 
  );
}
  
export default PokemonList;